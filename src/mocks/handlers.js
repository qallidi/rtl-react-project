import {rest} from "msw";
import {USERS} from "./Users";

// define all api Mocks in handlers
export const handlers = [rest.get('/fetch', (req, res, context) => {
    // Check if the user is authenticated in this session
    const isAuthenticated = sessionStorage.getItem('is-authenticated')
    if (!isAuthenticated) {
        // If not authenticated, respond with a 403 error
        return res(context.status(403), context.json({
            errorMessage: 'Not authorized',
        }),)
    } else {
        return res(// Respond with a 200 status code
            context.body(USERS), context.status(200));
    }
}), rest.post('/login', (req, res, context) => {
    // Persist user's authentication in the session
    sessionStorage.setItem("is-authenticated", "true");
    return res(// Respond with a 200 status code
        context.status(200));
}), rest.get('/save', (req, res, context) => {
    return res(// Respond with a 500 status code
        context.status(500));
})]
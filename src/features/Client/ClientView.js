import React, {useEffect} from 'react';
import {useDispatch} from "react-redux";

function ClientView() {
    const dispatch = useDispatch();
    useEffect(function login() {

    }, []);

    useEffect(function save() {

    }, []);

    useEffect(function fetch() {

    }, []);

    return (<div>
        <button role={"test-login"}>Login</button>
        <button role={"test-save"}>Save</button>
        <button role={"test-fetch"}>Fetch</button>
        {/* Body */}
    </div>);
}

export default ClientView;